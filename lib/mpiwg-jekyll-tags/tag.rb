module Jekyll
  class RenderIntroBlock < Liquid::Tag

    def initialize(tag_name, text, tokens)
      super
      @text = text
    end

    require "kramdown"
    def render(context)
      "<p class='intro'>#{ @text }</p>"
    end

  end
end

Liquid::Template.register_tag('intro', Jekyll::RenderIntroBlock)


module Jekyll
  class RenderAccordeonBlock < Liquid::Block

    def initialize(tag_name, raw_params, tokens)
      @raw_params = raw_params
      super
    end

    require "kramdown"
    def render(context)
      text = super
      title = @raw_params
      "<details>
          <summary>#{ title }</summary>
            <div>#{ Kramdown::Document.new(text).to_html }</div>
        </details>"
    end
    
  end
end

Liquid::Template.register_tag('accordeon', Jekyll::RenderAccordeonBlock)