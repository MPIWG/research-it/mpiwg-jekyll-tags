# MPIWG Jekyll Tags

> 💎 Custom functionality for MPIWG Jekyll Microsites

## Development

To release a new version do the following:

- increase version number in lib/mpiwg-jekyll-tags/version.rb
- add release notes to HISTORY.md
- build new gem: `gem build mpiwg-jekyll-tags.gemspec`
- push new version to rubygems.org: `gem push mpiwg-jekyll-tags-${version}.gem`
