lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "mpiwg-jekyll-tags/version"
Gem::Specification.new do |spec|
  spec.name          = "mpiwg-jekyll-tags"
  spec.summary       = "Custom tags for MPIWG Jekyll Microsites"
  spec.description   = "Custom tags for MPIWG Jekyll Microsites"
  spec.version       = MpiwgJekyllTags::VERSION
  spec.authors       = ["Florian Kräutli"]
  spec.email         = ["fkraeutli@mpiwg-berlin.mpg.de"]
  spec.homepage      = "http://www.mpiwg-berlin.mpg.de"
  spec.licenses      = ["MIT"]
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r!^(test|spec|features)/!)  }
  spec.require_paths = ["lib"]
  spec.add_dependency "jekyll", "~> 4.0"
  spec.add_development_dependency "rake", "~> 11.0"
  spec.add_development_dependency "rspec", "~> 3.5"
  spec.add_development_dependency "rubocop", "~> 0.52"
end
